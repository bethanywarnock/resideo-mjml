This repo is for all self-service email campaign files.

For a new campaign cd into ./mjml and then cd into the appropriate campaign group folder.

To transpile the campaign into html run "npm run build-emails --emails={name_of_email_file}"
ex. npm run build-emails --emails=prescription_approval_request


An html file with be generated in the dist folder, the body of which can be copied into the ESP (Adobe Campaign Standard) as either an email, or as a part of the template.

BW - 8/18/22
